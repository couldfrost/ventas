var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();

import Venta from '../Venta.js';
import Producto from '../Producto.js';
import Servicio from '../Servicio.js';
import Articulo from '../Articulo.js';
describe('venta', function() {
   
   
    it('total de venta sin producto deberia ser 0',function(){
     let venta=new Venta();
    expect(venta.Total()).equal(1);
    });
    it('total de venta con un servicio deberia darme sel total ',function(){
        let servicio=new Servicio("mantenimiento",12);
        let venta=new Venta();
        venta.add(servicio);
        expect(venta.Total()).equal(12);
    });
    it('total de venta con un articulo deberia darme sel total ',function(){
        let articulo=new Articulo("martillo",6);
        let venta=new Venta();
        venta.add(articulo);
       expect(venta.Total()).equal(6);
    });
    it('total de venta con un articulo y servicio deberia darme sel total ',function(){
        let servicio=new Servicio("mantenimiento",12);
        let articulo=new Articulo("martillo",6);
        let venta=new Venta();
        venta.add(servicio);
        venta.add(articulo);
       expect(venta.Total()).equal(18);
    });

   

    
});
